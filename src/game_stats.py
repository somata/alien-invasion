from settings import Settings


class GameStats:
    """跟踪游戏的统计信息"""

    def __init__(self, ai_settings: Settings):
        self.ai_settings = ai_settings
        self.ships_left = ai_settings.ship_limit
        self.game_active = False

        self.score = 0
        self.high_score = 0
        self.level = 1

        self.reset_stats()

    def reset_stats(self):
        self.ships_left = self.ai_settings.ship_limit
        self.score = 0
        self.level = 1
