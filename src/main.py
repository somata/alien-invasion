from time import sleep

import pygame
from pygame.sprite import Group

from ship import Ship
from button import Button
from game_stats import GameStats
import game_functions as game_func
from scoreboard import Scoreboard
from runtime_content import RuntimeContent
from settings import Settings


def run_game():
    # 初始化游戏并创建一个屏幕对象
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode((
        ai_settings.screen_width, ai_settings.screen_height))
    pygame.display.set_caption("Alien Invasion")

    play_button = Button(ai_settings, screen, "Play")

    ship = Ship(ai_settings, screen)  # 创建一艘飞船
    bullets = Group()   # 创建子弹
    aliens = Group()    # 外星人编组
    stats = GameStats(ai_settings)
    scoreboard = Scoreboard(ai_settings, screen, stats)

    runtime_content = RuntimeContent(ai_settings, stats, screen,    # 组装成一个数据集方便传参
                                     ship, aliens, bullets, play_button, scoreboard)

    game_func.create_fleet(runtime_content)

    # 开始游戏循环主程序
    while True:
        game_func.check_events(runtime_content)

        if stats.game_active:
            ship.update()
            game_func.update_bullets(runtime_content)
            game_func.update_aliens(runtime_content)

        game_func.update_screen(runtime_content)
        sleep(1/120)


if __name__ == "__main__":
    run_game()
