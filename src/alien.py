import pygame
from pygame import Surface
from pygame.sprite import Sprite

from settings import Settings


class Alien(Sprite):
    """表示单个外星人的类"""

    def __init__(self, ai_settings: Settings, screen: Surface):
        super().__init__()
        self.screen = screen
        self.ai_settings = ai_settings

        # 加载外星人图片 并设置rect属性
        self.image = pygame.image.load('../images/alien.bmp')
        self.rect = self.image.get_rect()

        # 初始位置于左上角
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        # 存储准确位置
        self.x = float(self.rect.x)

    def blitme(self):
        self.screen.blit(self.image, self.rect)

    def update(self, *args, **kwargs):
        self.x += self.ai_settings.alien_speed_factor * \
            self.ai_settings.fleet_direction
        self.rect.x = self.x

    def check_edges(self):
        screen_rect = self.screen.get_rect()
        if self.rect.right >= screen_rect.right:
            return True
        elif self.rect.left <= 0:
            return True
