from dataclasses import dataclass

from pygame import Surface
from pygame.sprite import Group

from ship import Ship
from button import Button
from game_stats import GameStats
from scoreboard import Scoreboard
from settings import Settings


@dataclass
class RuntimeContent:
    """程序运行过程中传递的变量"""
    ai_settings: Settings
    stats: GameStats
    screen: Surface
    ship: Ship
    aliens: Group
    bullets: Group
    play_button: Button
    scoreboard: Scoreboard
