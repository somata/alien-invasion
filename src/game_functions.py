import sys
from time import sleep

import pygame
from pygame.event import Event

from alien import Alien
from bullet import Bullet
from runtime_content import RuntimeContent
from settings import Settings


def start_game(runtime_content: RuntimeContent):
    """开始游戏"""
    runtime_content.ai_settings.initialize_dynamic_settings()
    runtime_content.stats.game_active = True
    runtime_content.stats.reset_stats()
    pygame.mouse.set_visible(False)

    # 清空外星人列表和子弹列表
    runtime_content.aliens.empty()
    runtime_content.bullets.empty()

    # 重记分数
    runtime_content.scoreboard.prep_score()
    runtime_content.scoreboard.prep_high_score()
    runtime_content.scoreboard.prep_level()
    runtime_content.scoreboard.prep_ships()

    # 新建外星人
    create_fleet(runtime_content)
    runtime_content.ship.center_ship()


def start_new_level(runtime_content: RuntimeContent):
    """开始新一轮"""
    runtime_content.bullets.empty()
    runtime_content.ai_settings.increase_speed()
    runtime_content.stats.level += 1
    runtime_content.scoreboard.prep_level()
    create_fleet(runtime_content)


def ship_hit(runtime_content: RuntimeContent):
    """响应被外星人撞到的飞船"""
    if runtime_content.stats.ships_left > 0:
        runtime_content.stats.ships_left -= 1

        runtime_content.scoreboard.prep_ships()

        runtime_content.aliens.empty()
        runtime_content.bullets.empty()

        create_fleet(runtime_content)
        runtime_content.ship.center_ship()

        sleep(0.5)
    else:
        runtime_content.stats.game_active = False
        pygame.mouse.set_visible(True)


def fire_bullet(runtime_content: RuntimeContent):
    """每到达限制, 发射子弹"""
    if len(runtime_content.bullets) < runtime_content.ai_settings.bullets_allowed:
        new_bullet = Bullet(runtime_content.ai_settings,
                            runtime_content.screen, runtime_content.ship)
        runtime_content.bullets.add(new_bullet)


def check_keydown_event(event: Event, runtime_content: RuntimeContent):
    """响应按键"""
    match event.key:
        case pygame.K_RIGHT:
            runtime_content.ship.moving_right = True
        case pygame.K_LEFT:
            runtime_content.ship.moving_left = True
        case pygame.K_SPACE:
            fire_bullet(runtime_content)
        case pygame.K_p:
            start_game(runtime_content)


def check_keyup_event(event: Event, runtime_content: RuntimeContent):
    """响应松开"""
    match event.key:
        case pygame.K_RIGHT:
            runtime_content.ship.moving_right = False
        case pygame.K_LEFT:
            runtime_content.ship.moving_left = False


def check_play_button(mouse_x: int, mouse_y: int, runtime_content: RuntimeContent):
    """检测用户点击范围是否为按钮"""
    button_clicked = runtime_content.play_button.rect.collidepoint(
        mouse_x, mouse_y)

    if button_clicked and not runtime_content.stats.game_active:
        start_game(runtime_content)


def check_events(runtime_content: RuntimeContent):
    """响应键盘和鼠标事件"""
    for event in pygame.event.get():
        match event.type:
            case pygame.QUIT:     # 退出事件
                sys.exit()
            case pygame.KEYDOWN:  # 键盘按下事件
                check_keydown_event(event, runtime_content)
            case pygame.KEYUP:
                check_keyup_event(event, runtime_content)
            case pygame.MOUSEBUTTONDOWN:
                mouse_x, mouse_y = pygame.mouse.get_pos()
                check_play_button(mouse_x, mouse_y, runtime_content)


def check_fleet_direction(runtime_content: RuntimeContent):
    """将整群外星人下移并改变他们的方向"""
    for alien in runtime_content.aliens.sprites():
        alien.rect.y += runtime_content.ai_settings.fleet_drop_speed
    runtime_content.ai_settings.fleet_direction *= -1


def check_fleet_edges(runtime_content: RuntimeContent):
    """有外星人到达边缘时删除"""
    for alien in runtime_content.aliens.sprites():
        if alien.check_edges():
            check_fleet_direction(runtime_content)
            break


def check_bullet_alien_collisions(runtime_content: RuntimeContent):
    """响应子弹和外星人的碰撞事件"""
    # 碰撞判断
    collisions = pygame.sprite.groupcollide(runtime_content.bullets,
                                            runtime_content.aliens, True, True)

    if collisions:
        for aliens in collisions.values():
            runtime_content.stats.score += (runtime_content.ai_settings.alien_points *
                                            len(aliens))
        runtime_content.scoreboard.prep_score()
        check_high_score(runtime_content)

    if not runtime_content.aliens:
        start_new_level(runtime_content)


def check_aliens_botton(runtime_content: RuntimeContent):
    """检测是否有外星人到达了屏幕底端"""
    screen_rect = runtime_content.screen.get_rect()
    for alien in runtime_content.aliens.sprites():
        if alien.rect.bottom >= screen_rect.bottom:
            ship_hit(runtime_content)
            break


def check_high_score(runtime_content: RuntimeContent):
    """检查是否诞生最高分"""
    if runtime_content.stats.score > runtime_content.stats.high_score:
        runtime_content.stats.high_score = runtime_content.stats.score
        runtime_content.scoreboard.prep_high_score()


def update_bullets(runtime_content: RuntimeContent):
    """更新子弹的位置并删除已消失子弹"""
    # 更新子弹的位置
    runtime_content.bullets.update()

    check_bullet_alien_collisions(runtime_content)

    # 删除已消失的子弹
    for bullet in runtime_content.bullets.copy():
        if bullet.rect.bottom <= 0:
            runtime_content.bullets.remove(bullet)


def update_aliens(runtime_content: RuntimeContent):
    check_fleet_edges(runtime_content)
    runtime_content.aliens.update()

    # 玩家与飞船碰撞
    if pygame.sprite.spritecollideany(runtime_content.ship, runtime_content.aliens):
        ship_hit(runtime_content)

    check_aliens_botton(runtime_content)


def update_screen(runtime_content: RuntimeContent):
    """更新屏幕上的图像, 并切换到新屏幕"""
    runtime_content.screen.fill(runtime_content.ai_settings.bg_color)

    # 绘制子弹
    for bullet in runtime_content.bullets.sprites():
        bullet.draw_bullet()

    runtime_content.ship.blitme()
    runtime_content.aliens.draw(runtime_content.screen)

    # 显示得分
    runtime_content.scoreboard.show_score()

    if not runtime_content.stats.game_active:
        runtime_content.play_button.draw_button()
    # 让最近绘制的屏幕可见
    pygame.display.flip()


def get_number_aliens_x(ai_settings: Settings, alien_width):
    """计算每行可以容纳多少个外星人"""
    available_space_x = ai_settings.screen_width - alien_width * 2
    numbers_aliens_x = int(available_space_x / (2 * alien_width))
    return numbers_aliens_x


def get_number_rows(ai_settings: Settings, ship_height, alien_height):
    """计算每行可以容纳多少个外星人"""
    available_space_y = ai_settings.screen_height - \
        (3*alien_height) - ship_height
    number_rows = int(available_space_y / (2 * alien_height))
    return number_rows


def create_alien(runtime_content: RuntimeContent, alien_number: int, row_number):
    """创建一个外星人实例"""
    alien = Alien(runtime_content.ai_settings, runtime_content.screen)
    alien_width = alien.rect.width
    alien.x = alien_width + 2 * alien_width * alien_number
    alien.rect.x = alien.x
    alien.rect.y = alien.rect.height + 2 * alien.rect.height * row_number
    runtime_content.aliens.add(alien)


def create_fleet(runtime_content: RuntimeContent):
    """创建外星人群"""
    alien = Alien(runtime_content.ai_settings,
                  runtime_content.screen)  # 该对象用于获取实际宽度
    alien_width = alien.rect.width
    numbers_aliens_x = get_number_aliens_x(
        runtime_content.ai_settings, alien_width)
    number_rows = get_number_rows(
        runtime_content.ai_settings, runtime_content.ship.rect.height, alien.rect.height)

    for alien_number in range(numbers_aliens_x):
        for row_number in range(number_rows):
            create_alien(runtime_content, alien_number, row_number)
