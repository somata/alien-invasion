class Settings:
    """存储《外星人入侵》的所有设置的类"""

    def __init__(self):
        # 屏幕参数设置
        self.screen_width = 1200
        self.screen_height = 700
        self.bg_color = 230, 230, 230

        # 飞船的设置
        self.ship_speed_factor = 1.5
        self.ship_limit = 3

        # 子弹设置
        self.bullet_speed_factor = 3
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = 60, 60, 60
        self.bullets_allowed = 10

        # 外星人设置
        self.alien_speed_factor = 1
        self.alien_points = 50
        self.fleet_drop_speed = 10
        self.fleet_direction = 1

        # 加速游戏
        self.speedup_scale = 1.1

        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        """初始化动态设置"""
        self.ship_speed_factor = 1.5
        self.bullet_speed_factor = 3
        self.alien_speed_factor = 1
        self.alien_points = 50

        self.fleet_direction = 1

    def increase_speed(self):
        self.ship_speed_factor *= self.speedup_scale
        self.bullet_speed_factor *= self.speedup_scale
        self.alien_speed_factor *= self.speedup_scale
        self.alien_points = int(self.speedup_scale * self.alien_points)
